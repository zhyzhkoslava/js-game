import { controls } from '../../constants/controls';

import Player from './fighterPlayer';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const firstPlayer = new Player(firstFighter);
    const secondPlayer = new Player(secondFighter);

    const keyDownListener = (event) => {
      checkKeyPress(event.code, firstPlayer, secondPlayer);

      if (firstPlayer.currentHealth <= 0) {
        removeKeyListeners();
        resolve(secondFighter);
      } else if (secondPlayer.currentHealth <= 0) {
        removeKeyListeners();
        resolve(firstFighter);
      }
    };
    document.addEventListener('keydown', keyDownListener);

    const keyUpListener = (event) => {
      if (event.code === controls.PlayerOneBlock && firstPlayer.blocking) {
        firstPlayer.blocking = false;
      } else if (event.code === controls.PlayerTwoBlock && secondPlayer.blocking) {
        secondPlayer.blocking = false;
      } else {
        // do nothing
      }
    };
    document.addEventListener('keyup', keyUpListener);

    const removeKeyListeners = () => {
      document.removeEventListener('keydown', keyDownListener);
      document.removeEventListener('keyup', keyUpListener);
    };
  });
}

export function getCriticalHit(fighter) {
  return fighter.attack * 2;
}

export function getDamage(attacker, defender) {
  // return damage
  if (attacker.blocking) {
    return 0;
  }

  if (!defender.blocking) {
    const damage = getHitPower(attacker) - getBlockPower(defender);
    return damage > 0 ? damage : 0;
  } else {
    return 0;
  }
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomNumber(1, 2);
  const { attack } = fighter;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomNumber(1, 2);
  const { defense } = fighter;
  return defense * dodgeChance;
}

const getRandomNumber = (min, max) => {
  return Math.random() * (max - min) + min;
};

const checkKeyPress = (keyCode, firstFighter, secondFighter) => {
  switch (keyCode) {
    case controls.PlayerOneAttack:
      const secondPlayerDamageDealt = getDamage(firstFighter, secondFighter);
      secondFighter.currentHealth -= secondPlayerDamageDealt;

      updateHealthIndicator(secondFighter, 'right');

      break;
    case controls.PlayerTwoAttack:
      const firstPlayerDealt = getDamage(secondFighter, firstFighter);
      firstFighter.currentHealth -= firstPlayerDealt;

      updateHealthIndicator(firstFighter, 'left');

      break;
    case controls.PlayerOneBlock:
      if (!firstFighter.blocking) {
        firstFighter.blocking = true;
      }
      break;
    case controls.PlayerTwoBlock:
      if (!secondFighter.blocking) {
        secondFighter.blocking = true;
      }
      break;
  }
};

const updateHealthIndicator = (fighter, side) => {
  document.getElementById(`${side}-fighter-indicator`).style.width = `${Math.round(
    (fighter.currentHealth / fighter.initialHealth) * 100
  )}%`;
};

